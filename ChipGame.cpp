#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "chrono"
#include "DCore.h"

#ifdef DEBUG
#include <cstdio>
#endif

DCore Core;

void RenderThread()
{
	while(true)
	{
		Core.RenderTick();
	}
}

int main()
{
#ifdef DEBUG
	stdio_init_all();
#endif

	multicore_launch_core1(RenderThread);
	auto PreTime = std::chrono::high_resolution_clock::now();

	while(true)
	{
		const auto CurrentTime = std::chrono::high_resolution_clock::now();
		std::chrono::duration<float> DeltaTime = CurrentTime - PreTime;

		Core.Tick(DeltaTime.count());
		PreTime = CurrentTime;
	}
}
