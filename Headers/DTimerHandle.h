#pragma once

#include "Delegates/DDelegate.h"
#include "pico/float.h"

DECLARE_DELEGATE(OnTimerSignature);

class DTimerHandle
{
public:
    friend class DTimerManager;

    void Tick(float DeltaTime);

    const bool operator==(const DTimerHandle &Other) const
    {
        return OnTimer == Other.OnTimer;
    }

    bool IsActive() const;

    void Clear();

private:
    class DTimerManager *TimerManager = nullptr;

    OnTimerSignature OnTimer;

    float TargetTime;

    float CurrentTime = 0;

    bool bIsLopped = false;

    template <typename UserClass>
    void InitTimer(class DTimerManager *InTimerManager, UserClass *InObject, Func<UserClass> InCallback, const float InTime, const bool IsLoop)
    {
        TimerManager = InTimerManager;
        TargetTime = InTime;
        CurrentTime = 0;
        bIsLopped = IsLoop;

        OnTimer.Bind(InObject, InCallback);
    }    
};
