#pragma once

#include "pico/stdlib.h"
#include "DTimerHandle.h"
#include <list>

class DTimerManager
{
public:
    DTimerManager() = default;

    void Tick(float DeltaTime);

    template <typename UserClass>
    void SetTimer(DTimerHandle &InHandle, UserClass *InObject, Func<UserClass> InCallback, const double InTime, const double IsLoop = false)
    {
        ClearTimer(InHandle);

        InHandle.InitTimer(this, InObject, InCallback, InTime, IsLoop);
        HandleList.push_back(&InHandle);
    }

    void ClearTimer(DTimerHandle &InTimerHandle);

private:
    std::list<DTimerHandle*> HandleList;
};