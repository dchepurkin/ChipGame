#pragma once

#include "DTimerManager.h"
#include "hardware/adc.h"
#include "DMenu.h"

DECLARE_DELEGATE(OnQuitGameSignature);

class DGame
{
public:
	OnQuitGameSignature OnQuitGame;

	explicit DGame(class DController* InController, class DScreen* InScreen)
			: Controller(InController),
			  Screen(InScreen),
			  GameOverMenu(DMenu(*Screen))
	{
		adc_init();
		adc_gpio_init(28);
		adc_select_input(2);

		GameOverMenu.AddMenu("������ � ������");
		GameOverMenu.AddMenu("�����");
	}

	virtual ~DGame();

	virtual void StartGame();

	virtual void Render() {}

	virtual void GameOver();

	void SetPause(const bool IsPaused);

	class DController* GetController() const { return Controller; }

	class DScreen* GetScreen() const { return Screen; }

	int GetGridSize() const { return GridSize; }

	int GetRandomInt(const int InMin, const int InMax) const;

protected:
	class DController* Controller = nullptr;

	class DScreen* Screen = nullptr;

	DMenu GameOverMenu;

	DTimerHandle GameTickTimerHandle;

	bool bIsRenderUpdated = false;

	bool bIsGameOver = false;

	int GridSize = 20;

	float FrameRate = 0.15f;

	virtual void GameTick() = 0;

	void UpdateScreen() { bIsRenderUpdated = true; }

	void ShowGameOverWindow();
};