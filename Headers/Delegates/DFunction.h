#pragma once

template <class UserClass>
using Func = void (UserClass::*)();

template <class UserClass, class ParamType>
using FuncOneParam = void (UserClass::*)(ParamType);

class DObjectMethod
{
public:
    virtual ~DObjectMethod() = default;

    virtual void Execute() = 0;

    virtual bool operator==(void *InObject) = 0;
};

template <typename UserClass>
class TObjectMethod : public DObjectMethod
{
public:
    TObjectMethod(UserClass *InObject, Func<UserClass> InMethod)
        : Object(InObject),
          Method(InMethod) {}

    virtual void Execute() override
    {
        if (!Object)
            return;

        (Object->*Method)();
    }

    virtual bool operator==(void *InObject) override
    {
        return InObject == Object;
    }

private:
    UserClass *Object = nullptr;

    Func<UserClass> Method = nullptr;
};

/////////////////////////////////////////////////////////////////////////////////////////

template <typename ParamType>
class DObjectMethodOneParam
{
public:
    virtual ~DObjectMethodOneParam() = default;

    virtual void Execute(ParamType ParamValue) = 0;

    virtual bool operator==(void *InObject) = 0;
};

template <typename UserClass, typename ParamType>
class TObjectMethodOneParam : public DObjectMethodOneParam<ParamType>
{
public:
    TObjectMethodOneParam(UserClass *InObject, FuncOneParam<UserClass, ParamType> InMethod)
        : Object(InObject),
          Method(InMethod) {}

    void Execute(ParamType ParamValue) override
    {
        (Object->*Method)(ParamValue);
    }

    bool operator==(void *InObject) override
    {
        return InObject == Object;
    }

private:
    UserClass *Object = nullptr;

    FuncOneParam<UserClass, ParamType> Method = nullptr;
};