#pragma once

#include <list>
#include <algorithm>
#include "Delegates/DFunction.h"

#define DECLARE_DELEGATE(DelegateName) using DelegateName = DDelegate;

#define DECLARE_DELEGATE_OneParam(DelegateName, ParamType) using DelegateName = DDelegateOneParam<ParamType>;

class DDelegate
{
public:
    DDelegate() = default;

    const bool operator==(const DDelegate &Other) const
    {
        return Observer == Other.Observer;
    }

    template <typename UserClass>
    void Bind(UserClass *InObject, Func<UserClass> InCallback)
    {
        if (InObject == nullptr)
            return;

        Clear();

        Observer = new TObjectMethod<UserClass>(InObject, InCallback);
    }

    void Broadcast();

    void Clear();

    bool IsBinded() const { return Observer != nullptr; }

private:
    DObjectMethod *Observer = nullptr;
};

////////////////////////////////////////////////////////////////////////////////////

template <typename ParamType>
class DDelegateOneParam
{
public:
    DDelegateOneParam() = default;

    const bool operator==(const DDelegateOneParam &Other) const
    {
        return Observer == Other.Observer;
    }

    template <typename UserClass>
    void Bind(UserClass *InObject, FuncOneParam<UserClass, ParamType> InCallback)
    {
        if (InObject == nullptr)
            return;

        Clear();

        Observer = new TObjectMethodOneParam<UserClass, ParamType>(InObject, InCallback);
    }

    void Broadcast(ParamType InValue)
    {
        if (IsBinded())
        {
            Observer->Execute(InValue);
        }
    }

    void Clear()
    {
        if (IsBinded())
        {
            delete Observer;
            Observer = nullptr;
        }
    }

    bool IsBinded() const { return Observer != nullptr; }

private:
    DObjectMethodOneParam<ParamType> *Observer = nullptr;
};
