#pragma once

#include "DGame.h"
#include "DTimerHandle.h"
#include "SnakeGame/DSnakeHead.h"
#include "SnakeGame/DSnakeFood.h"

class DSnakeGame : public DGame
{
public:
	DSnakeGame() = delete;

	DSnakeGame(class DController* InController, class DScreen* InScreen);

	~DSnakeGame() override;

	void StartGame() override;

	void Render() override;

	void GameOver() override;

protected:
	void GameTick() override;

private:
	DSnakeHead Head;

	DSnakeFood Food;

	const uint InitSnakeSize = 5;

	const uint WinSnakeSize = 344;

	void OnUpButtonCallback(bool IsPressed);

	void OnDownButtonCallback(bool IsPressed);

	void OnLeftButtonCallback(bool IsPressed);

	void OnRightButtonCallback(bool IsPressed);

	void MoveFoodToRandomLocation();

	void OpenWinnerWindow();

	void InitGame();
};