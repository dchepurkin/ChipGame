#pragma once

#include "Screen/DScreenTypes.h"

class DSnakeElement
{
public:
    DSnakeElement(class DScreen *InScreen, const int InGridSize);

	~DSnakeElement();

    void SetLocation(const DVector2D &InNewLocation);

    const DVector2D &GetLocation() const { return Location; }

    void AddChild(DSnakeElement *InSnakeElement);

    void Render();

    bool IsBlockingHit(const DVector2D &InLocation) const;

private:
    class DScreen *Screen = nullptr;

    DVector2D Location{-1, -1};

    DSnakeElement *Next = nullptr;

    int GridSize;
};