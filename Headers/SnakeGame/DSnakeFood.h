#pragma once

#include "Screen/DScreenTypes.h"

class DSnakeFood
{
public:
    void Init(class DScreen *InScreen, uint InGridSize);

    void Render();

    void SetLocation(const DVector2D &InLocation);

    const DVector2D &GetLocation() const { return Location; }

private:
    bool IsChangedLocation = false;
    
    DVector2D Location{-1, -1};

    class DScreen *Screen = nullptr;

    uint GridSize = 0;
};