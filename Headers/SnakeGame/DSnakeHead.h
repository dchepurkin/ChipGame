#pragma once

#include "pico/stdlib.h"
#include "Screen/DScreenTypes.h"
#include "Delegates/DDelegate.h"
#include <map>

DECLARE_DELEGATE(OnDeathSignature);
DECLARE_DELEGATE(OnMoveFinishedSignature);

enum class EDirection
{
	Up,
	Down,
	Right,
	Left
};

class DSnakeHead
{
public:
	OnDeathSignature OnDeath;

	OnMoveFinishedSignature OnMove;

	DSnakeHead() = default;

	void Init(class DSnakeGame* Ingame);

	void Render();

	void Move();

	void Kill();

	void SetDirection(const EDirection InDirection);

	bool IsBlockingHit(const DVector2D& InLocation) const;

	void SpawnElement();

	const DVector2D& GetLocation() const { return Location; }

	uint GetSize() const { return SnakeSize; }

private:
	class DScreen* Screen = nullptr;

	class DSnakeElement* Next = nullptr;

	class DSnakeElement* Last = nullptr;

	const uint KillGridValue = 1;

	const uint FoodGridValue = 2;

	int GridSize;

	uint SnakeSize = 0;

	DVector2D Location;

	EDirection CurrentDirection = EDirection::Up;

	EDirection LastDirection = CurrentDirection;

	std::map<EDirection, DVector2D> DirectionMap{
			{  EDirection::Up   , DVector2D{0, -1}}
			, {EDirection::Down , DVector2D{0, 1}}
			, {EDirection::Left , DVector2D{-1, 0}}
			, {EDirection::Right, DVector2D{1, 0}}};
};