#pragma once

#include "pico/stdlib.h"
#include "DTimerManager.h"
#include "Controller/DController.h"
#include "Screen/DScreen.h"
#include "DGame.h"
#include "DMenu.h"

class DCore
{
public:
	DCore();

	const DScreen& GetScreen() const { return Screen; }

	DCore(const DCore&) = delete;

	DCore& operator=(const DCore&) = delete;

	void RenderTick();

	void Tick(float DeltaTime);

	DTimerManager& GetTimerManager() { return TimerManager; }

private:
	DScreen Screen;

	DMenu StartMenu{Screen};

	DTimerManager TimerManager;

	DController Controller{this};

	DGame* Game = nullptr;

	void StartSnakeGame();

	void StartTetrisGame();

	void DownButton_Callback(bool IsPressed);

	void UpButton_Callback(bool IsPressed);

	void RightButton_Callback(bool IsPressed);

	void ShowStartScreen();

	void OnQuitGame_Callback();
};