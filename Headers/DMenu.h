#pragma once

#include "pico/stdlib.h"
#include "Screen/DScreen.h"

class DMenu
{
public:
	explicit DMenu(DScreen& InScreen)
			: Screen(InScreen) {}

	void AddMenu(const char* InText);

	void Show(const DVector2D& InPosition, uint16_t InTextColor, uint16_t InBackgroundColor);

	void SwitchCursorDown();

	void SwitchCursorUp();

	uint GetCursorPosition() const { return CurrentPosition; }

private:
	std::map<uint, const char*> MenuText;

	DScreen& Screen;

	DVector2D Position{};

	uint16_t BackgroundColor = BLUE;

	uint CursorRadius = 4;

	uint CurrentPosition = 0;

	DVector2D CursorOffset{0, 40};

	DVector2D TextOffset{15, 40};

	void SetCursorPosition(uint InPosition);
};