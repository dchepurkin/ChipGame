#pragma once

#define BLACK 0x0000
#define WHITE 0xFFFF
#define RED 0xF800
#define GREEN 0x07E0
#define BLUE 0x1F5F
#define DARK_BLUE 0x237B
#define GRAY 0x9D33
#define CYAN 0x07FF
#define MAGENTA 0xF81F
#define YELLOW 0xFFE0
#define ORANGE 0xFC00
#define PINK 0xF9CD
#define DARK_PINK 0xA0C4
#define LIGHT_GREEN 0x3FED
#define DARK_GREEN 0x2D60
#define PURPLE 0x8B3F
#define DARK_PURPLE 0x7115
#define LIGHT_BLUE 0x637F
#define GREEN_01 0x04E0
#define GREEN_02 0x0280

#define START_MENU_CURSOR_COLOR RED

#define GAMEOVER_WINDOW_BORDER_COLOR BLUE
#define GAMEOVER_WINDOW_FILL_COLOR 0x07BF

#define SNAKE_BACKGROUND_COLOR GRAY
#define SNAKE_BORDER_COLOR 0x2965
#define SNAKE_HEAD_COLOR GREEN
#define SNAKE_HEAD_BORDER_COLOR 0x04E0
#define SNAKE_ELEMENT_COLOR RED
#define SNAKE_ELEMENT_BORDER_COLOR 0x9800
#define SNAKE_FOOD_COLOR 0x001F