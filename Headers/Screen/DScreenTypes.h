#pragma once

#include "pico/stdlib.h"

struct DVector2D
{
	DVector2D(const int InX = 0, const int InY = 0)
			: X(InX),
			  Y(InY) {}

	bool IsXEqual(const DVector2D& Other) const
	{
		return X == Other.X;
	}

	bool IsYEqual(const DVector2D& Other) const
	{
		return Y == Other.Y;
	}

	bool operator==(const DVector2D& Other) const
	{
		return IsXEqual(Other) && IsYEqual(Other);
	}

	bool operator!=(const DVector2D& Other) const
	{
		return !(*this == Other);
	}

	void Set(const int InX, const int InY)
	{
		X = InX;
		Y = InY;
	}

	DVector2D operator-(const DVector2D& Other) const
	{
		return DVector2D(X - Other.X, Y - Other.Y);
	}

	DVector2D operator+(const DVector2D& Other) const
	{
		return DVector2D(X + Other.X, Y + Other.Y);
	}

	DVector2D operator+(const int InValue) const
	{
		return DVector2D(X + InValue, Y + InValue);
	}

	DVector2D operator-(const int InValue) const
	{
		return DVector2D(X - InValue, Y - InValue);
	}

	DVector2D operator/(uint InValue) const
	{
		return DVector2D(X / InValue, Y / InValue);
	}

	DVector2D operator*(uint InValue) const
	{
		return DVector2D(X * InValue, Y * InValue);
	}

	DVector2D operator*(DVector2D Other) const
	{
		return DVector2D(X * Other.X, Y * Other.Y);
	}

	DVector2D YMultiply(uint InValue) const
	{
		return DVector2D(X, Y * InValue);
	}

	DVector2D XMultiply(uint InValue) const
	{
		return DVector2D(X * InValue, Y);
	}

	DVector2D YAdd(uint InValue) const
	{
		return DVector2D(X, Y + InValue);
	}

	DVector2D XAdd(uint InValue) const
	{
		return DVector2D(X + InValue, Y);
	}

	int X = 0;

	int Y = 0;
};

struct DScreenInfo
{
	DScreenInfo(const uint8_t InPinDIN = 7,
				const uint8_t InPinCLK = 6,
				const uint8_t InPinCS = 5,
				const uint8_t InPinDC = 4,
				const uint8_t InPinRESET = 8,
				const uint8_t InPinLED = 3,
				const double InSerialClkDiv = 1.f,
				const DVector2D& InScreenSize = DVector2D{320, 480})
			: PinDIN(InPinDIN),
			  PinCLK(InPinCLK),
			  PinCS(InPinCS),
			  PinDC(InPinDC),
			  PinRESET(InPinRESET),
			  PinLED(InPinLED),
			  SerialClkDiv(InSerialClkDiv),
			  Size(InScreenSize)
	{
	}

	uint8_t PinDIN = 7;

	uint8_t PinCLK = 6;

	uint8_t PinCS = 5;

	uint8_t PinDC = 4;

	uint8_t PinRESET = 8;

	uint8_t PinLED = 3;

	double SerialClkDiv = 1.0;

	DVector2D Size{320, 480};
};