#pragma once

#include "DGame.h"
#include <map>
#include "Screen/DColors.h"

class DTetrisGame : public DGame
{
public:
	DTetrisGame() = delete;

	DTetrisGame(class DController* InController, class DScreen* InScreen)
			: DGame(InController, InScreen) {}

	~DTetrisGame() override;

	void StartGame() override;

	void Render() override;

	void GameOver() override;

protected:
	void GameTick() override;

private:
	class DFigure* CurrentFigure = nullptr;

	DTimerHandle DropFigureTimerHandle;

	DTimerHandle GameOverTimerHandle;

	bool bFullScreenUpdate = false;

	bool bIsDropped = false;

	std::map<uint, std::pair<uint16_t, uint32_t>> ColorsMap = {
			{  1, {DARK_BLUE, BLUE}}
			, {2, {DARK_PINK, PINK}}
			, {3, {DARK_GREEN, LIGHT_GREEN}}
			, {4, {DARK_PURPLE, PURPLE}}
			, {5, {SNAKE_FOOD_COLOR, LIGHT_BLUE}}
			, {6, {GREEN_02, GREEN_01}}
			, {7, {ORANGE, YELLOW}}
	};

	uint Grid[345] = {
			//0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12,13,14
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //0
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //1
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //2
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //3
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //4
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //5
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //6
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //7
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //8
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //9
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //10
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //11
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //12
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //13
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //14
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //15
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //16
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //17
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //18
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //19
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //20
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, //21
			0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  //22
	};

	void OnUpButtonCallback(bool IsPressed);

	void OnDownButtonCallback(bool IsPressed);

	void OnLeftButtonCallback(bool IsPressed);

	void OnRightButtonCallback(bool IsPressed);

	void SpawnFigure();

	void DropFigure();

	void DropLoop_Callback();

	void ClearFullLines();

	void InitGame();
};