#pragma once

#include "pico/stdio.h"
#include "Screen/DScreen.h"

class DFigure
{
public:
	DFigure(const DVector2D& InSpawnPoint)
			: SpawnPoint(InSpawnPoint)
	{
		Location = SpawnPoint;
		PreLocation = Location;
	}

	virtual ~DFigure() = default;

	DVector2D SpawnPoint;

	DVector2D Location;

	DVector2D PreLocation;

	virtual void Move(const DVector2D& InOffset)
	{
		PreLocation = Location;
		Location = Location + InOffset;
	}

	virtual void Render(DScreen* InScreen) = 0;

	virtual void Rotate() {}

	virtual bool CanRotate(const uint* InGrid) const { return true; }

	virtual bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const { return false; }

	virtual void UpdateGrid(uint* InGrid) const = 0;

protected:
	int Rotation = 0;

	int PreRotation = Rotation;

	virtual bool HaveCollision(const DVector2D& InLocation, int InRotation, const uint* InGrid) const { return false; }
};

class DSquare : public DFigure
{
public:
	explicit DSquare()
			: DFigure({7, 0}) {}

	void Render(DScreen* InScreen) override
	{
		DVector2D Point = PreLocation * 20 + DVector2D{10, 10};
		InScreen->DrawFillRect(Point, {20, 20}, SNAKE_BACKGROUND_COLOR);

		Point = Location * 20 + DVector2D{10, 10};
		InScreen->DrawRect(Point, {20, 20}, 4, DARK_BLUE, true, BLUE);
	}

	bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const override
	{
		const auto CheckedLocation = Location + InOffset;
		if(CheckedLocation.X < 0 || CheckedLocation.X > 14 || CheckedLocation.Y > 22) { return true; }

		return InGrid[CheckedLocation.Y * 15 + CheckedLocation.X] != 0;
	}

	void UpdateGrid(uint* InGrid) const override
	{
		InGrid[Location.Y * 15 + Location.X] = 1;
	}
};

class DLine : public DFigure
{
public:
	explicit DLine()
			: DFigure({7, 0}) {}

	void Rotate() override
	{
		PreRotation = Rotation;
		Rotation = (++Rotation) % 2;
		PreLocation = Location;
	}

	void Render(DScreen* InScreen) override
	{
		DVector2D Point = PreLocation * 20 + DVector2D{10, 10};

		switch(PreRotation)
		{
			case 0:
			{
				InScreen->DrawFillRect(Point, {20, 80}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 1:
			{
				InScreen->DrawFillRect(Point, {80, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}
		}

		for(int i = 0; i < 4; ++i)
		{
			Point = Location * 20 + DVector2D{10, 10};
			switch(Rotation)
			{
				case 0:
				{
					InScreen->DrawRect(Point.YAdd(i * 20), {20, 20}, 4, DARK_PINK, true, PINK);
					break;
				}

				case 1:
				{
					InScreen->DrawRect(Point.XAdd(i * 20), {20, 20}, 4, DARK_PINK, true, PINK);
					break;
				}
			}
		}

		PreRotation = Rotation;
	}

	bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const override
	{
		return HaveCollision(Location + InOffset, Rotation, InGrid);
	}

	bool HaveCollision(const DVector2D& InLocation, int InRotation, const uint* InGrid) const override
	{
		if(InLocation.X < 0) { return true; }

		if(InRotation == 0)
		{
			if(InLocation.X > 14 || InLocation.Y + 3 > 22) { return true; }

			for(int i = 0; i < 4; ++i)
			{
				if(InGrid[(InLocation.Y + i) * 15 + InLocation.X] != 0) { return true; }
			}

			return InGrid[(InLocation.Y + 3) * 15 + InLocation.X] != 0;
		}
		else if(InRotation == 1)
		{
			if(InLocation.X + 3 > 14 || InLocation.Y > 22) { return true; }

			for(int i = 0; i < 4; ++i)
			{
				if(InGrid[InLocation.Y * 15 + InLocation.X + i] != 0) { return true; }
			}
		}

		return false;
	}

	void UpdateGrid(uint* InGrid) const override
	{
		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 4; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X] = 2;
				}
				break;
			}

			case 1:
			{
				for(int i = 0; i < 4; ++i)
				{
					InGrid[Location.Y * 15 + Location.X + i] = 2;
				}
				break;
			}
		}
	}

	bool CanRotate(const uint* InGrid) const override
	{
		return !HaveCollision(Location, (Rotation + 1) % 2, InGrid);
	}
};

class DLFigure : public DFigure
{
public:
	DLFigure()
			: DFigure({7, 0}) {}

	void Render(DScreen* InScreen) override
	{
		DVector2D Point = PreLocation * 20 + DVector2D{10, 10};

		switch(PreRotation)
		{
			case 0:
			{
				InScreen->DrawFillRect(Point, {20, 60}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point + DVector2D{20, 40}, {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 1:
			{
				InScreen->DrawFillRect(Point, {60, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.YAdd(20), {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 2:
			{
				InScreen->DrawFillRect(Point.XAdd(20), {20, 60}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point, {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 3:
			{
				InScreen->DrawFillRect(Point.YAdd(20), {60, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.XAdd(40), {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}
		}

		Point = Location * 20 + DVector2D{10, 10};

		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.YAdd(i * 20), {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				}

				InScreen->DrawRect(Point + DVector2D{20, 40}, {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				break;
			}

			case 1:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(i * 20), {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				}

				InScreen->DrawRect(Point.YAdd(20), {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				break;
			}

			case 2:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(20).YAdd(i * 20), {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				}

				InScreen->DrawRect(Point, {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				break;
			}

			case 3:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(i * 20).YAdd(20), {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				}

				InScreen->DrawRect(Point.XAdd(40), {20, 20}, 4, DARK_GREEN, true, LIGHT_GREEN);
				break;
			}
		}

		PreRotation = Rotation;
	}

	bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const override
	{
		return HaveCollision(Location + InOffset, Rotation, InGrid);
	}

	void UpdateGrid(uint* InGrid) const override
	{
		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X] = 3;
				}

				InGrid[(Location.Y + 2) * 15 + Location.X + 1] = 3;
				break;
			}

			case 1:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[Location.Y * 15 + Location.X + i] = 3;
				}

				InGrid[(Location.Y + 1) * 15 + Location.X] = 3;
				break;
			}

			case 2:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X + 1] = 3;
				}

				InGrid[Location.Y * 15 + Location.X] = 3;
				break;
			}

			case 3:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + 1) * 15 + Location.X + i] = 3;
				}

				InGrid[Location.Y * 15 + Location.X + 2] = 3;
				break;
			}

			default: { break; }
		}
	}

	bool HaveCollision(const DVector2D& InLocation, int InRotation, const uint* InGrid) const override
	{
		if(InLocation.X < 0) { return true; }

		switch(InRotation)
		{
			case 0:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X] != 0) { return true; }
				}

				return InGrid[(InLocation.Y + 2) * 15 + InLocation.X + 1] != 0;
			}

			case 1:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[InLocation.Y * 15 + InLocation.X + i] != 0) { return true; }
				}

				return InGrid[(InLocation.Y + 1) * 15 + InLocation.X] != 0;
			}

			case 2:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X + 1] != 0) { return true; }
				}

				return InGrid[InLocation.Y * 15 + InLocation.X] != 0;
			}

			case 3:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + 1) * 15 + InLocation.X + i] != 0) { return true; }
				}

				return InGrid[InLocation.Y * 15 + InLocation.X + 2] != 0;
			}

			default: { break; }
		}

		return false;
	}

	bool CanRotate(const uint* InGrid) const override
	{
		return !HaveCollision(Location, (Rotation + 1) % 4, InGrid);
	}

	void Rotate() override
	{
		PreRotation = Rotation;
		Rotation = (++Rotation) % 4;
		PreLocation = Location;
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DLMirrorFigure : public DFigure
{
public:
	DLMirrorFigure()
			: DFigure({7, 0}) {}

	void Render(DScreen* InScreen) override
	{
		DVector2D Point = PreLocation * 20 + DVector2D{10, 10};

		switch(PreRotation)
		{
			case 0:
			{
				InScreen->DrawFillRect(Point.XAdd(20), {20, 60}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.YAdd(40), {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 1:
			{
				InScreen->DrawFillRect(Point, {20, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.YAdd(20), {60, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 2:
			{
				InScreen->DrawFillRect(Point, {20, 60}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.XAdd(20), {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 3:
			{
				InScreen->DrawFillRect(Point, {60, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.XAdd(40).YAdd(20), {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}
		}

		Point = Location * 20 + DVector2D{10, 10};

		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(20).YAdd(i * 20), {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				}

				InScreen->DrawRect(Point.YAdd(40), {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				break;
			}

			case 1:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.YAdd(20).XAdd(i * 20), {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				}

				InScreen->DrawRect(Point, {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				break;
			}

			case 2:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.YAdd(i * 20), {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				}

				InScreen->DrawRect(Point.XAdd(20), {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				break;
			}

			case 3:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(i * 20), {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				}

				InScreen->DrawRect(Point.XAdd(40).YAdd(20), {20, 20}, 4, DARK_PURPLE, true, PURPLE);
				break;
			}
		}

		PreRotation = Rotation;
	}

	bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const override
	{
		return HaveCollision(Location + InOffset, Rotation, InGrid);
	}

	void UpdateGrid(uint* InGrid) const override
	{
		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X + 1] = 4;
				}

				InGrid[(Location.Y + 2) * 15 + Location.X] = 4;
				break;
			}

			case 1:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + 1) * 15 + Location.X + i] = 4;
				}

				InGrid[Location.Y * 15 + Location.X] = 4;
				break;
			}

			case 2:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X] = 4;
				}

				InGrid[Location.Y * 15 + Location.X + 1] = 4;
				break;
			}

			case 3:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[Location.Y * 15 + Location.X + i] = 4;
				}

				InGrid[(Location.Y + 1) * 15 + Location.X + 2] = 4;
				break;
			}

			default: { break; }
		}
	}

	bool HaveCollision(const DVector2D& InLocation, int InRotation, const uint* InGrid) const override
	{
		if(InLocation.X < 0) { return true; }

		switch(InRotation)
		{
			case 0:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X + 1] != 0) { return true; }
				}

				return InGrid[(InLocation.Y + 2) * 15 + InLocation.X] != 0;
			}

			case 1:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + 1) * 15 + InLocation.X + i] != 0) { return true; }
				}

				return InGrid[InLocation.Y * 15 + InLocation.X] != 0;
			}

			case 2:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X] != 0) { return true; }
				}

				return InGrid[InLocation.Y * 15 + InLocation.X + 1] != 0;
			}

			case 3:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[InLocation.Y * 15 + InLocation.X + i] != 0) { return true; }
				}

				return InGrid[(InLocation.Y + 1) * 15 + InLocation.X + 2] != 0;
			}

			default: { break; }
		}

		return false;
	}

	bool CanRotate(const uint* InGrid) const override
	{
		return !HaveCollision(Location, (Rotation + 1) % 4, InGrid);
	}

	void Rotate() override
	{
		PreRotation = Rotation;
		Rotation = (++Rotation) % 4;
		PreLocation = Location;
	}
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DZFigure : public DFigure
{
public:
	DZFigure()
			: DFigure({6, 0}) {}

	void Render(DScreen* InScreen) override
	{
		DVector2D Point = PreLocation * 20 + DVector2D{10, 10};

		switch(PreRotation)
		{
			case 0:
			{
				InScreen->DrawFillRect(Point, {40, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.XAdd(20).YAdd(20), {40, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 1:
			{
				InScreen->DrawFillRect(Point.YAdd(20), {20, 40}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.XAdd(20), {20, 40}, SNAKE_BACKGROUND_COLOR);
				break;
			}
		}

		Point = Location * 20 + DVector2D{10, 10};

		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 2; ++i)
				{
					InScreen->DrawRect(Point.XAdd(i * 20), {20, 20}, 4, SNAKE_FOOD_COLOR, true, LIGHT_BLUE);
					InScreen->DrawRect(Point.XAdd(i * 20 + 20).YAdd(20), {20, 20}, 4, SNAKE_FOOD_COLOR, true, LIGHT_BLUE);
				}
				break;
			}

			case 1:
			{
				for(int i = 0; i < 2; ++i)
				{
					InScreen->DrawRect(Point.YAdd(20 + i * 20), {20, 20}, 4, SNAKE_FOOD_COLOR, true, LIGHT_BLUE);
					InScreen->DrawRect(Point.YAdd(i * 20).XAdd(20), {20, 20}, 4, SNAKE_FOOD_COLOR, true, LIGHT_BLUE);
				}
				break;
			}
		}

		PreRotation = Rotation;
	}

	bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const override
	{
		return HaveCollision(Location + InOffset, Rotation, InGrid);
	}

	void UpdateGrid(uint* InGrid) const override
	{
		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 2; ++i)
				{
					InGrid[Location.Y * 15 + Location.X + i] = 5;
					InGrid[(Location.Y + 1) * 15 + Location.X + i + 1] = 5;
				}
				break;
			}

			case 1:
			{
				for(int i = 0; i < 2; ++i)
				{
					InGrid[(Location.Y + 1 + i) * 15 + Location.X] = 5;
					InGrid[(Location.Y + i) * 15 + Location.X + 1] = 5;
				}
				break;
			}

			default: { break; }
		}
	}

	bool HaveCollision(const DVector2D& InLocation, int InRotation, const uint* InGrid) const override
	{
		if(InLocation.X < 0) { return true; }

		switch(InRotation)
		{
			case 0:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 2; ++i)
				{
					if(InGrid[InLocation.Y * 15 + InLocation.X + i] != 0) { return true; }
					if(InGrid[(InLocation.Y + 1) * 15 + InLocation.X + i + 1] != 0) { return true; }
				}

				break;
			}

			case 1:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 2; ++i)
				{
					if(InGrid[(InLocation.Y + 1 + i) * 15 + InLocation.X] != 0) { return true; }
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X + 1] != 0) { return true; }
				}

				break;
			}

			default: { break; }
		}

		return false;
	}

	bool CanRotate(const uint* InGrid) const override
	{
		return !HaveCollision(Location, (Rotation + 1) % 2, InGrid);
	}

	void Rotate() override
	{
		PreRotation = Rotation;
		Rotation = (++Rotation) % 2;
		PreLocation = Location;
	}
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DZMirrorFigure : public DFigure
{
public:
	DZMirrorFigure()
			: DFigure({6, 0}) {}

	void Render(DScreen* InScreen) override
	{
		DVector2D Point = PreLocation * 20 + DVector2D{10, 10};

		switch(PreRotation)
		{
			case 0:
			{
				InScreen->DrawFillRect(Point.XAdd(20), {40, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.YAdd(20), {40, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 1:
			{
				InScreen->DrawFillRect(Point.XAdd(20).YAdd(20), {20, 40}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point, {20, 40}, SNAKE_BACKGROUND_COLOR);
				break;
			}
		}

		Point = Location * 20 + DVector2D{10, 10};

		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 2; ++i)
				{
					InScreen->DrawRect(Point.XAdd(i * 20 + 20), {20, 20}, 4, GREEN_02, true, GREEN_01);
					InScreen->DrawRect(Point.XAdd(i * 20).YAdd(20), {20, 20}, 4, GREEN_02, true, GREEN_01);
				}
				break;
			}

			case 1:
			{
				for(int i = 0; i < 2; ++i)
				{
					InScreen->DrawRect(Point.YAdd(i * 20), {20, 20}, 4, GREEN_02, true, GREEN_01);
					InScreen->DrawRect(Point.YAdd(i * 20 + 20).XAdd(20), {20, 20}, 4, GREEN_02, true, GREEN_01);
				}
				break;
			}
		}

		PreRotation = Rotation;
	}

	bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const override
	{
		return HaveCollision(Location + InOffset, Rotation, InGrid);
	}

	void UpdateGrid(uint* InGrid) const override
	{
		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 2; ++i)
				{
					InGrid[Location.Y * 15 + Location.X + i + 1] = 6;
					InGrid[(Location.Y + 1) * 15 + Location.X + i] = 6;
				}
				break;
			}

			case 1:
			{
				for(int i = 0; i < 2; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X] = 6;
					InGrid[(Location.Y + i + 1) * 15 + Location.X + 1] = 6;
				}
				break;
			}

			default: { break; }
		}
	}

	bool HaveCollision(const DVector2D& InLocation, int InRotation, const uint* InGrid) const override
	{
		if(InLocation.X < 0) { return true; }

		switch(InRotation)
		{
			case 0:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 2; ++i)
				{
					if(InGrid[InLocation.Y * 15 + InLocation.X + i + 1] != 0) { return true; }
					if(InGrid[(InLocation.Y + 1) * 15 + InLocation.X + i] != 0) { return true; }
				}

				break;
			}

			case 1:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 2; ++i)
				{
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X] != 0) { return true; }
					if(InGrid[(InLocation.Y + i + 1) * 15 + InLocation.X + 1] != 0) { return true; }
				}

				break;
			}

			default: { break; }
		}

		return false;
	}

	bool CanRotate(const uint* InGrid) const override
	{
		return !HaveCollision(Location, (Rotation + 1) % 2, InGrid);
	}

	void Rotate() override
	{
		PreRotation = Rotation;
		Rotation = (++Rotation) % 2;
		PreLocation = Location;
	}
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DTFigure : public DFigure
{
public:
	DTFigure()
			: DFigure({6, 0}) {}

	void Render(DScreen* InScreen) override
	{
		DVector2D Point = PreLocation * 20 + DVector2D{10, 10};

		switch(PreRotation)
		{
			case 0:
			{
				InScreen->DrawFillRect(Point.XAdd(20), {20, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.YAdd(20), {60, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 1:
			{
				InScreen->DrawFillRect(Point, {20, 60}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.XAdd(20).YAdd(20), {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 2:
			{
				InScreen->DrawFillRect(Point.XAdd(20).YAdd(20), {20, 20}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point, {60, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}

			case 3:
			{
				InScreen->DrawFillRect(Point.XAdd(20), {20, 60}, SNAKE_BACKGROUND_COLOR);
				InScreen->DrawFillRect(Point.YAdd(20), {20, 20}, SNAKE_BACKGROUND_COLOR);
				break;
			}
		}

		Point = Location * 20 + DVector2D{10, 10};

		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(i * 20).YAdd(20), {20, 20}, 4, ORANGE, true, YELLOW);
				}

				InScreen->DrawRect(Point.XAdd(20), {20, 20}, 4, ORANGE, true, YELLOW);
				break;
			}

			case 1:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.YAdd(i * 20), {20, 20}, 4, ORANGE, true, YELLOW);
				}

				InScreen->DrawRect(Point.YAdd(20).XAdd(20), {20, 20}, 4, ORANGE, true, YELLOW);
				break;
			}

			case 2:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(i * 20), {20, 20}, 4, ORANGE, true, YELLOW);
				}

				InScreen->DrawRect(Point.XAdd(20).YAdd(20), {20, 20}, 4, ORANGE, true, YELLOW);
				break;
			}

			case 3:
			{
				for(int i = 0; i < 3; ++i)
				{
					InScreen->DrawRect(Point.XAdd(20).YAdd(i * 20), {20, 20}, 4, ORANGE, true, YELLOW);
				}

				InScreen->DrawRect(Point.YAdd(20), {20, 20}, 4, ORANGE, true, YELLOW);
				break;
			}
		}

		PreRotation = Rotation;
	}

	bool IsBlockingHit(const DVector2D& InOffset, const uint* InGrid) const override
	{
		return HaveCollision(Location + InOffset, Rotation, InGrid);
	}

	void UpdateGrid(uint* InGrid) const override
	{
		switch(Rotation)
		{
			case 0:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + 1) * 15 + Location.X + i] = 7;
				}

				InGrid[Location.Y * 15 + Location.X + 1] = 7;
				break;
			}

			case 1:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X] = 7;
				}

				InGrid[(Location.Y + 1) * 15 + Location.X + 1] = 7;
				break;
			}

			case 2:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[Location.Y * 15 + Location.X + i] = 7;
				}

				InGrid[(Location.Y + 1) * 15 + Location.X + 1] = 7;
				break;
			}

			case 3:
			{
				for(int i = 0; i < 3; ++i)
				{
					InGrid[(Location.Y + i) * 15 + Location.X + 1] = 7;
				}

				InGrid[(Location.Y + 1) * 15 + Location.X] = 7;
				break;
			}

			default: { break; }
		}
	}

	bool HaveCollision(const DVector2D& InLocation, int InRotation, const uint* InGrid) const override
	{
		if(InLocation.X < 0) { return true; }

		switch(InRotation)
		{
			case 0:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + 1) * 15 + InLocation.X + i] != 0) { return true; }
				}

				return InGrid[InLocation.Y * 15 + InLocation.X + 1] != 0;
			}

			case 1:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X] != 0) { return true; }
				}

				return InGrid[(InLocation.Y + 1) * 15 + InLocation.X + 1] != 0;
			}

			case 2:
			{
				if(InLocation.X + 2 > 14 || InLocation.Y + 1 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[InLocation.Y * 15 + InLocation.X + i] != 0) { return true; }
				}

				return InGrid[(InLocation.Y + 1) * 15 + InLocation.X + 1] != 0;
			}

			case 3:
			{
				if(InLocation.X + 1 > 14 || InLocation.Y + 2 > 22) { return true; }

				for(int i = 0; i < 3; ++i)
				{
					if(InGrid[(InLocation.Y + i) * 15 + InLocation.X + 1] != 0) { return true; }
				}

				return InGrid[(InLocation.Y + 1) * 15 + InLocation.X] != 0;
			}

			default: { break; }
		}

		return false;
	}

	bool CanRotate(const uint* InGrid) const override
	{
		return !HaveCollision(Location, (Rotation + 1) % 4, InGrid);
	}

	void Rotate() override
	{
		PreRotation = Rotation;
		Rotation = (++Rotation) % 4;
		PreLocation = Location;
	}
};