#pragma once

#include "pico/stdio.h"

class DObject
{
public:
    DObject() = default;

    virtual ~DObject() = default;

    virtual void Tick(float DeltaTime) {}
};