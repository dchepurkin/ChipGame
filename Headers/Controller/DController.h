#pragma once

#include "pico/stdlib.h"
#include "DTimerManager.h"
#include "Controller/DButton.h"

class DController
{
public:
    DButton UpButton{14, this};

    DButton RightButton{27, this};

    DButton DownButton{26, this};

    DButton LeftButton{15, this};

    explicit DController(class DCore *InGameCore);

    class DCore *GetCore() const { return Core; }

    DTimerManager& GetTimerManager();

    void Tick(float DeltaTime);

    void ClearButtonBindings();

private:
    class DCore *Core;
};