#pragma once

#include "pico/stdlib.h"
#include "Delegates/DDelegate.h"
#include "DTimerHandle.h"

DECLARE_DELEGATE_OneParam(OnButtonPressedSignature, bool);

class DButton
{
public:
    DButton() = delete;

    explicit DButton(const uint InPin, class DController *InController);

    template <typename UserClass>
    void Bind(UserClass *InObject, FuncOneParam<UserClass, bool> InCallback)
    {
        OnButtonPressed.Bind(InObject, InCallback);
        StartTimer();
    }

    void Unbind();

    bool IsPressed() const { return bIsPressed; }

private:
    OnButtonPressedSignature OnButtonPressed;

    class DController *Controller;

    uint Pin;

    float TriggerDelay = 0.05f;

    bool bIsPressed = false;

    DTimerHandle PressTimerHandle;

    void PressCallback();

    void StartTimer();
};