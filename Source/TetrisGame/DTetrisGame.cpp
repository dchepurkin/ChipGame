#include "TetrisGame/DTetrisGame.h"
#include "Controller/DController.h"
#include "Screen/DScreen.h"
#include "TetrisGame/DFigure.h"

DTetrisGame::~DTetrisGame()
{
}

void DTetrisGame::StartGame()
{
	DVector2D Point{0, 0};
	Screen->DrawRect({0, 0}, Screen->GetScreenSize(), GridSize / 2, SNAKE_BORDER_COLOR, true, SNAKE_BACKGROUND_COLOR);

	Controller->UpButton.Bind(this, &DTetrisGame::OnUpButtonCallback);
	Controller->DownButton.Bind(this, &DTetrisGame::OnDownButtonCallback);
	Controller->LeftButton.Bind(this, &DTetrisGame::OnLeftButtonCallback);
	Controller->RightButton.Bind(this, &DTetrisGame::OnRightButtonCallback);

	FrameRate = 0.5f;

	DGame::StartGame();

	InitGame();
}

void DTetrisGame::InitGame()
{
	for(auto& Value : Grid)
	{
		Value = 0;
	}

	bIsGameOver = false;
	SpawnFigure();
	bFullScreenUpdate = true;
}

void DTetrisGame::Render()
{
	if(bIsRenderUpdated)
	{
		if(bFullScreenUpdate)
		{
			DVector2D Point;
			for(int y = 0; y < 23; ++y)
			{
				for(int x = 0; x < 15; ++x)
				{
					Point.Set(x * GridSize + GridSize / 2, y * GridSize + GridSize / 2);
					const auto GridValue = Grid[y * 15 + x];
					if(GridValue != 0)
					{
						Screen->DrawRect(Point, DVector2D{GridSize, GridSize}, 4, ColorsMap[GridValue].first, true, ColorsMap[GridValue].second);
					}
					else
					{
						Screen->DrawFillRect(Point, DVector2D{GridSize, GridSize}, SNAKE_BACKGROUND_COLOR);
					}
				}
			}
			bFullScreenUpdate = false;
		}

		if(CurrentFigure)
		{
			CurrentFigure->Render(Screen);
		}

		bIsRenderUpdated = false;
	}
}

void DTetrisGame::GameOver()
{
	if(CurrentFigure)
	{
		delete CurrentFigure;
		CurrentFigure = nullptr;
	}

	bIsGameOver = true;

	Controller->GetTimerManager().SetTimer(GameOverTimerHandle, static_cast<DGame*>(this), &DTetrisGame::ShowGameOverWindow, 0.5);
}

void DTetrisGame::GameTick()
{
	if(bIsGameOver) { return; }

	if(CurrentFigure && !bIsDropped)
	{
		if(CurrentFigure->IsBlockingHit({0, 1}, Grid))
		{
			CurrentFigure->UpdateGrid(Grid);
			ClearFullLines();
			SpawnFigure();
		}
		else
		{
			CurrentFigure->Move({0, 1});
		}

		bIsRenderUpdated = true;
	}
}

void DTetrisGame::OnUpButtonCallback(bool IsPressed)
{
	if(IsPressed)
	{
		if(bIsGameOver)
		{
			GameOverMenu.SwitchCursorUp();
			return;
		}
		if(!bIsDropped && CurrentFigure && CurrentFigure->CanRotate(Grid))
		{
			CurrentFigure->Rotate();
			bIsRenderUpdated = true;
		}
	}
}

void DTetrisGame::OnDownButtonCallback(bool IsPressed)
{
	if(IsPressed)
	{
		if(bIsGameOver)
		{
			GameOverMenu.SwitchCursorDown();
			return;
		}

		if(!bIsDropped)
		{
			DropFigure();
		}
	}
}

void DTetrisGame::OnLeftButtonCallback(bool IsPressed)
{
	if(IsPressed && !bIsGameOver && !bIsDropped)
	{
		if(CurrentFigure && !CurrentFigure->IsBlockingHit({-1, 0}, Grid))
		{
			CurrentFigure->Move({-1, 0});
			bIsRenderUpdated = true;
		}
	}
}

void DTetrisGame::OnRightButtonCallback(bool IsPressed)
{
	if(IsPressed)
	{
		if(bIsGameOver)
		{
			switch(GameOverMenu.GetCursorPosition())
			{
				case 0:
				{
					InitGame();
					break;
				}
				case 1:
				{
					OnQuitGame.Broadcast();
					break;
				}
			}
			return;
		}

		if(!bIsDropped && CurrentFigure && !CurrentFigure->IsBlockingHit({1, 0}, Grid))
		{
			CurrentFigure->Move({1, 0});
			bIsRenderUpdated = true;
		}
	}
}

void DTetrisGame::SpawnFigure()
{
	if(CurrentFigure)
	{
		delete CurrentFigure;
		CurrentFigure = nullptr;
	}

	const auto RandInt = GetRandomInt(0, 7);

	switch(RandInt)
	{
		case 0:
		{
			CurrentFigure = new DSquare();
			break;
		}

		case 1:
		{
			CurrentFigure = new DLine();
			break;
		}

		case 2:
		{
			CurrentFigure = new DLFigure();
			break;
		}

		case 3:
		{
			CurrentFigure = new DLMirrorFigure();
			break;
		}

		case 4:
		{
			CurrentFigure = new DZFigure();
			break;
		}

		case 5:
		{
			CurrentFigure = new DZMirrorFigure();
			break;
		}

		case 6:
		{
			CurrentFigure = new DTFigure();
			break;
		}

		default: { break; }
	}

	if(CurrentFigure && CurrentFigure->IsBlockingHit({0, 0}, Grid))
	{
		GameOver();
	}
}

void DTetrisGame::DropFigure()
{
	if(CurrentFigure && !DropFigureTimerHandle.IsActive())
	{
		bIsDropped = true;
		Controller->GetTimerManager().SetTimer(DropFigureTimerHandle, this, &DTetrisGame::DropLoop_Callback, 0.01, true);
	}
}

void DTetrisGame::DropLoop_Callback()
{
	if(CurrentFigure)
	{
		if(CurrentFigure->IsBlockingHit({0, 1}, Grid))
		{
			Controller->GetTimerManager().ClearTimer(DropFigureTimerHandle);
			CurrentFigure->UpdateGrid(Grid);
			ClearFullLines();
			SpawnFigure();

			bIsDropped = false;
		}
		else
		{
			CurrentFigure->Move({0, 1});
		}

		bIsRenderUpdated = true;
	}
}

void DTetrisGame::ClearFullLines()
{
	for(int y = 0; y < 23; ++y)
	{
		bool bIsFullLine = true;
		for(int x = 0; x < 15; ++x)
		{
			if(Grid[y * 15 + x] == 0)
			{
				bIsFullLine = false;
				break;
			}
		}

		if(bIsFullLine)
		{
			for(int NewY = y; NewY >= 0; --NewY)
			{
				for(int x = 0; x < 15; ++x)
				{
					Grid[NewY * 15 + x] = NewY == 0 ? 0 : Grid[(NewY - 1) * 15 + x];
				}
			}
		}
	}

	bFullScreenUpdate = true;
	bIsRenderUpdated = true;
}


