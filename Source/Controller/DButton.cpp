#include "Controller/DButton.h"
#include "Controller/DController.h"

DButton::DButton(const uint InPin, class DController *InController)
{
    Pin = InPin;
    Controller = InController;

    gpio_init(Pin);
    gpio_set_dir(Pin, GPIO_IN);
}

void DButton::PressCallback()
{
    if (gpio_get(Pin) != bIsPressed)
    {
        bIsPressed = !bIsPressed;
        OnButtonPressed.Broadcast(bIsPressed);
    }
}

void DButton::StartTimer()
{
    Controller->GetTimerManager().SetTimer(PressTimerHandle, this, &DButton::PressCallback, TriggerDelay, true);
}

void DButton::Unbind()
{
    Controller->GetTimerManager().ClearTimer(PressTimerHandle);
}