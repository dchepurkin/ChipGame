#include "DTimerManager.h"
#include "DCore.h"

#include <stdio.h>

void DTimerManager::Tick(float DeltaTime)
{
    for (auto Handle : HandleList)
    {
        if (!Handle->IsActive())
        {
            continue;
        }

        Handle->Tick(DeltaTime);
    }
}

void DTimerManager::ClearTimer(DTimerHandle &InTimerHandle)
{
    InTimerHandle.Clear();
    HandleList.remove(&InTimerHandle);
}
