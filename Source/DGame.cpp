#include "DGame.h"
#include "Controller/DController.h"
#include "Screen/DScreen.h"
#include "DMenu.h"

void DGame::StartGame()
{
	SetPause(false);
}

void DGame::GameOver()
{
	bIsGameOver = true;
	ShowGameOverWindow();
}

void DGame::SetPause(const bool IsPaused)
{
	IsPaused
	? Controller->GetTimerManager().ClearTimer(GameTickTimerHandle)
	: Controller->GetTimerManager().SetTimer(GameTickTimerHandle, this, &DGame::GameTick, FrameRate, true);
}

int DGame::GetRandomInt(const int InMin, const int InMax) const
{
	srand(adc_read());
	return InMin + rand() % InMax;
}

void DGame::ShowGameOverWindow()
{
	const int WindowYPosition = 130;

	Screen->DrawRect({50, WindowYPosition}, {220, 170}, 4, DARK_BLUE, true, BLUE);
	Screen->PrintString("�� ���������", EFontSize::Font20, {70, WindowYPosition + 10}, RED, BLUE);
	Screen->DrawFillRect({100, WindowYPosition + 60}, {120, 4}, RED);

	GameOverMenu.SwitchCursorUp();
	GameOverMenu.Show({85, 210}, BLACK, BLUE);
}

DGame::~DGame()
{
	Controller->GetTimerManager().ClearTimer(GameTickTimerHandle);
	OnQuitGame.Clear();
}
