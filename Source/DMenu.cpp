#include "DMenu.h"

void DMenu::Show(const DVector2D& InPosition, uint16_t InTextColor, uint16_t InBackgroundColor)
{
	BackgroundColor = InBackgroundColor;
	Position = InPosition;

	for(const auto [MenuNum, Text] : MenuText)
	{
		Screen.PrintString(Text, EFontSize::Font14, Position + TextOffset.YMultiply(MenuNum), InTextColor, BackgroundColor);
	}

	SetCursorPosition(0);
}

void DMenu::SwitchCursorDown()
{
	SetCursorPosition(CurrentPosition + 1);
}

void DMenu::SwitchCursorUp()
{
	SetCursorPosition(CurrentPosition - 1);
}

void DMenu::SetCursorPosition(uint InPosition)
{
	if(InPosition < 0 || InPosition > MenuText.size() - 1) { return; }

	Screen.DrawFillCircle(Position + DVector2D(0, CursorRadius * 3) + CursorOffset * CurrentPosition, CursorRadius, BackgroundColor);
	CurrentPosition = InPosition;
	Screen.DrawFillCircle(Position + DVector2D(0, CursorRadius * 3) + CursorOffset * CurrentPosition, CursorRadius, START_MENU_CURSOR_COLOR);
}

void DMenu::AddMenu(const char* InText)
{
	MenuText.emplace(MenuText.size(), InText);
}
