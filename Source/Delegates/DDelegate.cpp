#include "Delegates/DDelegate.h"

void DDelegate::Broadcast()
{
    if (IsBinded())
    {
        Observer->Execute();
    }
}

void DDelegate::Clear()
{
    if (IsBinded())
    {
        delete Observer;
        Observer = nullptr;
    }
}