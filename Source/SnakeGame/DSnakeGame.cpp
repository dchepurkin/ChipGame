#include "SnakeGame/DSnakeGame.h"
#include "Controller/DController.h"
#include "Screen/DScreen.h"
#include "DMenu.h"

DSnakeGame::DSnakeGame(struct DController* InController, struct DScreen* InScreen)
		: DGame(InController, InScreen) {}

void DSnakeGame::StartGame()
{
	InitGame();

	Head.OnMove.Bind(static_cast<DGame*>(this), &DSnakeGame::UpdateScreen);
	Head.OnDeath.Bind(this, &DSnakeGame::GameOver);

	Controller->UpButton.Bind(this, &DSnakeGame::OnUpButtonCallback);
	Controller->DownButton.Bind(this, &DSnakeGame::OnDownButtonCallback);
	Controller->LeftButton.Bind(this, &DSnakeGame::OnLeftButtonCallback);
	Controller->RightButton.Bind(this, &DSnakeGame::OnRightButtonCallback);

	DGame::StartGame();
}

void DSnakeGame::InitGame()
{
	bIsGameOver = false;

	DVector2D Point{0, 0};
	Screen->DrawRect({0, 0}, Screen->GetScreenSize(), GridSize / 2, SNAKE_BORDER_COLOR, true, SNAKE_BACKGROUND_COLOR);

	Head.Init(this);
	for(uint i = 0; i < InitSnakeSize; ++i)
	{
		Head.SpawnElement();
	}

	Food.Init(Screen, GridSize);
	MoveFoodToRandomLocation();
}

void DSnakeGame::Render()
{
	if(bIsRenderUpdated)
	{
		Head.Render();
		Food.Render();
		bIsRenderUpdated = false;
	}
}

void DSnakeGame::GameOver()
{
	DGame::GameOver();
	Head.Kill();
}

void DSnakeGame::GameTick()
{
	if(bIsGameOver) { return; }

	Head.Move();

	if(Head.GetLocation() == Food.GetLocation())
	{
		Head.SpawnElement();

		if(Head.GetSize() == WinSnakeSize)
		{
			OpenWinnerWindow();
		}
		else
		{
			MoveFoodToRandomLocation();
		}
	}
}

void DSnakeGame::OnUpButtonCallback(bool IsPressed)
{
	if(!IsPressed)
	{
		return;
	}

	if(!bIsGameOver)
	{
		Head.SetDirection(EDirection::Up);
	}
	else
	{
		GameOverMenu.SwitchCursorUp();
	}
}

void DSnakeGame::OnDownButtonCallback(bool IsPressed)
{
	if(!IsPressed)
	{
		return;
	}

	if(!bIsGameOver)
	{
		Head.SetDirection(EDirection::Down);
	}
	else
	{
		GameOverMenu.SwitchCursorDown();
	}
}

void DSnakeGame::OnLeftButtonCallback(bool IsPressed)
{
	if(!IsPressed)
	{
		return;
	}

	if(!bIsGameOver)
	{
		Head.SetDirection(EDirection::Left);
	}
}

void DSnakeGame::OnRightButtonCallback(bool IsPressed)
{
	if(!IsPressed)
	{
		return;
	}

	if(!bIsGameOver)
	{
		Head.SetDirection(EDirection::Right);
	}
	else
	{
		switch(GameOverMenu.GetCursorPosition())
		{
			case 0:
			{
				InitGame();
				break;
			}
			case 1:
			{
				OnQuitGame.Broadcast();
				break;
			}
		}
	}
}

void DSnakeGame::MoveFoodToRandomLocation()
{
	while(true)
	{
		const auto X = GetRandomInt(1, 15);
		const auto Y = GetRandomInt(1, 23);

		DVector2D FoodLocation{X, Y};

		if(FoodLocation != Head.GetLocation() && !Head.IsBlockingHit(FoodLocation))
		{
			Food.SetLocation(FoodLocation);
			break;
		}
	}
}

void DSnakeGame::OpenWinnerWindow()
{
}

DSnakeGame::~DSnakeGame()
{
	Head.OnMove.Clear();
	Head.OnDeath.Clear();
}



