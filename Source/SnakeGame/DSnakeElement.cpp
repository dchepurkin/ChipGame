#include "SnakeGame/DSnakeElement.h"
#include "Screen/DScreen.h"

DSnakeElement::DSnakeElement(DScreen* InScreen, const int InGridSize)
{
	Screen = InScreen;
	GridSize = InGridSize;
}

void DSnakeElement::SetLocation(const DVector2D& InNewLocation)
{
	if(Next)
	{
		Next->SetLocation(Location);
	}

	Location = InNewLocation;
}

void DSnakeElement::AddChild(DSnakeElement* InSnakeElement)
{
	Next = InSnakeElement;
	Next->SetLocation(Location);
}

void DSnakeElement::Render()
{
	DVector2D Point = Location * GridSize - GridSize / 2;
	DVector2D Size{GridSize, GridSize};
	Screen->DrawRect(Point, Size, 2, SNAKE_ELEMENT_BORDER_COLOR, true, SNAKE_ELEMENT_COLOR);

	if(!Next)
	{
		Screen->DrawFillRect(Point, Size, SNAKE_BACKGROUND_COLOR);
	}
	else
	{
		Next->Render();
	}
}

bool DSnakeElement::IsBlockingHit(const DVector2D& InLocation) const
{
	return InLocation == Location || (Next ? Next->IsBlockingHit(InLocation) : false);
}

DSnakeElement::~DSnakeElement()
{
	if(Next)
	{
		delete Next;
		Next = nullptr;
	}
}
