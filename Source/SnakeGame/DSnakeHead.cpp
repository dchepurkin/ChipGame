#ifdef DEBUG

#include <cstdio>

#endif

#include "SnakeGame/DSnakeHead.h"
#include "SnakeGame/DSnakeGame.h"
#include "Screen/DScreen.h"
#include "SnakeGame/DSnakeElement.h"

void DSnakeHead::Init(DSnakeGame* InGame)
{
	if(!InGame)
	{
		return;
	}

	Screen = InGame->GetScreen();
	GridSize = InGame->GetGridSize();

	Location = Screen->GetScreenSize() / GridSize / 2;
}

void DSnakeHead::Render()
{
	DVector2D Point = Location * GridSize - GridSize / 2;
	DVector2D Size{GridSize, GridSize};
	Screen->DrawRect(Point, Size, 2, SNAKE_HEAD_BORDER_COLOR, true, SNAKE_HEAD_COLOR);

	if(Next)
	{
		Next->Render();
	}
}

void DSnakeHead::Move()
{
	if(IsBlockingHit(Location))
	{
		OnDeath.Broadcast();
		return;
	}

	if(Next)
	{
		Next->SetLocation(Location);
	}

	Location = Location + DirectionMap[CurrentDirection];
	LastDirection = CurrentDirection;

#ifdef DEBUG
	printf("X = %i, Y = %i\n", Location.X, Location.Y);
#endif

	OnMove.Broadcast();
}

void DSnakeHead::SetDirection(const EDirection InDirection)
{
	switch(InDirection)
	{
		case EDirection::Up:
		{
			if(CurrentDirection != EDirection::Down && LastDirection != EDirection::Down)
			{
				CurrentDirection = EDirection::Up;
			}
			break;
		}

		case EDirection::Down:
		{
			if(CurrentDirection != EDirection::Up && LastDirection != EDirection::Up)
			{
				CurrentDirection = EDirection::Down;
			}
			break;
		}

		case EDirection::Left:
		{
			if(CurrentDirection != EDirection::Right && LastDirection != EDirection::Right)
			{
				CurrentDirection = EDirection::Left;
			}
			break;
		}

		case EDirection::Right:
		{
			if(CurrentDirection != EDirection::Left && LastDirection != EDirection::Left)
			{
				CurrentDirection = EDirection::Right;
			}
			break;
		}
	}
}

void DSnakeHead::SpawnElement()
{
	if(const auto NewElement = new DSnakeElement(Screen, GridSize))
	{
		if(Last)
		{
			Last->AddChild(NewElement);
		}

		Last = NewElement;

		if(!Next)
		{
			Next = NewElement;
		}
	}

	++SnakeSize;
}

bool DSnakeHead::IsBlockingHit(const DVector2D& InLocation) const
{
	const auto WallsHit =
			InLocation.X < 1 || InLocation.X > Screen->GetWidth() / GridSize - 1 || InLocation.Y < 1 || InLocation.Y > Screen->GetHeight() / GridSize - 1;

	return WallsHit || (Next ? Next->IsBlockingHit(InLocation) : false);
}

void DSnakeHead::Kill()
{
	if(Next)
	{
		delete Next;
		Next = nullptr;
	}

	Last = nullptr;

	SnakeSize = 0;
	CurrentDirection = EDirection::Up;
	LastDirection = CurrentDirection;
}
