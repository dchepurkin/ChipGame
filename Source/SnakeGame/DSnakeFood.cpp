#include "pico/stdlib.h"
#include "SnakeGame/DSnakeFood.h"
#include "Screen/DScreen.h"

void DSnakeFood::Init(DScreen *InScreen, uint InGridSize)
{
    Screen = InScreen;
    GridSize = InGridSize;
}

void DSnakeFood::Render()
{
    if (!IsChangedLocation)
        return;

    DVector2D Point = Location * GridSize;
    Screen->DrawFillCircle(Point, GridSize / 2 - 1, SNAKE_FOOD_COLOR);
}

void DSnakeFood::SetLocation(const DVector2D &InLocation)
{
    Location = InLocation;
    IsChangedLocation = true;
}