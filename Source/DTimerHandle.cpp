#include "DTimerHandle.h"
#include "DTimerManager.h"

#include "DCore.h"

void DTimerHandle::Tick(float DeltaTime)
{
    CurrentTime = CurrentTime + DeltaTime;
    if (CurrentTime >= TargetTime)
    {
        OnTimer.Broadcast();

        if (bIsLopped)
        {
            CurrentTime = 0.f;
        }
        else
        {
            TimerManager->ClearTimer(*this);
        }
    }
}

bool DTimerHandle::IsActive() const
{
    return OnTimer.IsBinded();
}

void DTimerHandle::Clear()
{
    OnTimer.Clear();
}
