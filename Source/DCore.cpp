#include "DCore.h"
#include "SnakeGame/DSnakeGame.h"
#include "TetrisGame/DTetrisGame.h"
#include "hardware/watchdog.h"

DCore::DCore()
{
	StartMenu.AddMenu("������");
	StartMenu.AddMenu("������");

	ShowStartScreen();
}

void DCore::RenderTick()
{
	if(Game)
	{
		Game->Render();
	}
}

void DCore::Tick(float DeltaTime)
{
	TimerManager.Tick(DeltaTime);
	Controller.Tick(DeltaTime);
}

void DCore::StartSnakeGame()
{
	Game = new DSnakeGame{&Controller, &Screen};
	Game->OnQuitGame.Bind(this, &DCore::OnQuitGame_Callback);
	Game->StartGame();
}

void DCore::StartTetrisGame()
{
	Game = new DTetrisGame{&Controller, &Screen};
	Game->OnQuitGame.Bind(this, &DCore::OnQuitGame_Callback);
	Game->StartGame();
}

void DCore::DownButton_Callback(bool IsPressed)
{
	if(IsPressed)
	{
		StartMenu.SwitchCursorDown();
	}
}

void DCore::UpButton_Callback(bool IsPressed)
{
	if(IsPressed)
	{
		StartMenu.SwitchCursorUp();
	}
}

void DCore::RightButton_Callback(bool IsPressed)
{
	if(IsPressed)
	{
		switch(StartMenu.GetCursorPosition())
		{
			case 0:
			{
				StartSnakeGame();
				break;
			}

			case 1:
			{
				StartTetrisGame();
				break;
			}

			default: { break; }
		}
	}
}

void DCore::OnQuitGame_Callback()
{
	watchdog_reboot(0, 0, 0);
}

void DCore::ShowStartScreen()
{
	const int WindowYPosition = 130;

	Screen.FillScreen(GRAY);
	Screen.DrawRect({50, WindowYPosition}, {220, 170}, 4, DARK_BLUE, true, BLUE);
	Screen.PrintString("Chip Game", EFontSize::Font26, {75, WindowYPosition + 10}, RED, BLUE);
	Screen.DrawFillRect({100, WindowYPosition + 60}, {120, 4}, RED);

	StartMenu.Show({85, 210}, BLACK, BLUE);

	Controller.DownButton.Bind(this, &DCore::DownButton_Callback);
	Controller.UpButton.Bind(this, &DCore::UpButton_Callback);
	Controller.RightButton.Bind(this, &DCore::RightButton_Callback);
}


